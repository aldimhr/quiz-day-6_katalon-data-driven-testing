import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('PreCondition/Loggedin User'), [('Username') : 'standard_user', ('Password') : 'secret_sauce'], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.waitForElementPresent(findTestObject('Page_Inventory/btn_Menu cart'), GlobalVariable.G_Timeout)

WebUI.verifyElementPresent(findTestObject('Page_Inventory/btn_Menu cart'), GlobalVariable.G_Timeout)

WebUI.waitForElementPresent(findTestObject('Page_Inventory/btn_Menu'), GlobalVariable.G_Timeout)

WebUI.verifyElementPresent(findTestObject('Page_Inventory/btn_Menu'), GlobalVariable.G_Timeout)

WebUI.verifyElementPresent(findTestObject('Page_Inventory/btn_Menu cart'), GlobalVariable.G_Timeout)

WebUI.verifyElementPresent(findTestObject('Page_Inventory/link_Facebook'), GlobalVariable.G_Timeout)

WebUI.verifyElementPresent(findTestObject('Page_Inventory/link_LinkedIn'), GlobalVariable.G_Timeout)

WebUI.verifyElementPresent(findTestObject('Page_Inventory/link_Twitter'), GlobalVariable.G_Timeout)

WebUI.click(findTestObject('Page_Inventory/btn_Menu'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementPresent(findTestObject('Page_Inventory/Menu__btn_About'), GlobalVariable.G_Timeout)

WebUI.verifyElementPresent(findTestObject('Page_Inventory/Menu__btn_All Items'), GlobalVariable.G_Timeout)

WebUI.verifyElementPresent(findTestObject('Page_Inventory/Menu__btn_Logout'), GlobalVariable.G_Timeout)

WebUI.verifyElementPresent(findTestObject('Page_Inventory/Menu__btn_Reset App State'), GlobalVariable.G_Timeout)

WebUI.closeBrowser()

